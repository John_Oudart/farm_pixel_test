﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instantiator : MonoBehaviour {

    public GameObject ennemy;
    public float freq=0.5f;

    float base_time = 0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Time.time >= base_time+freq)
            {
                Instantiate(ennemy, new Vector3(this.transform.position.x  , this.transform.position.y + Random.Range(-3.0f, 3.0f) , this.transform.position.z), Quaternion.identity);
                base_time = Time.time;
            }
    }
}
