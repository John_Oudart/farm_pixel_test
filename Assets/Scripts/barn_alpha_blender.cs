﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barn_alpha_blender : MonoBehaviour {


    public GameObject barn1, barn2, barn3, barn4;
    public int delay=20;
    private int current_timer=0, current_phase=1;
     Color color_1, color_2, color_3, color_4;
    
    // Use this for initialization
    void Start () {
        color_1 = barn1.GetComponent<SpriteRenderer>().color;
        color_1.a = 1;

        barn1.GetComponent<SpriteRenderer>().color = color_1;

        color_2 = barn2.GetComponent<SpriteRenderer>().color;
        color_2.a = 0;
        barn2.GetComponent<SpriteRenderer>().color = color_2;


        color_3 = barn3.GetComponent<SpriteRenderer>().color;
        color_3.a = 0;
        barn3.GetComponent<SpriteRenderer>().color = color_3;

        color_4 = barn4.GetComponent<SpriteRenderer>().color;
        color_4.a = 0;
        barn4.GetComponent<SpriteRenderer>().color = color_4;

    }

    // Update is called once per frame
    void Update () {
		if (current_timer >= delay)
        {
            // do someting
            if (current_phase==1)
            {
                color_2.a +=0.1F;
                color_2.b = 255;
                color_2.r = 255;
                color_2.g = 255;
                barn2.GetComponent<SpriteRenderer>().color = color_2;

                color_1.b = 255;
                color_1.r = 255;
                color_1.g = 255;
                color_1.a = color_1.a -0.05F;
                barn1.GetComponent<SpriteRenderer>().color = color_1;
                if (color_1.a <= 0F)
                {
                    color_2.a =1F;
                    current_phase = 2;
                }
            }
            if (current_phase == 2)
            {
                color_3.a += 0.1F;
                color_3.b = 255;
                color_3.r = 255;
                color_3.g = 255;
                barn3.GetComponent<SpriteRenderer>().color = color_3;

                color_2.b = 255;
                color_2.r = 255;
                color_2.g = 255;
                color_2.a = color_2.a - 0.05F;
                barn2.GetComponent<SpriteRenderer>().color = color_2;
                if (color_2.a <= 0F)
                {
                    color_3.a = 1F;
                    current_phase = 3;
                }
            }
            if (current_phase == 3)
            {
                color_4.a += 0.1F;
                color_4.b = 255;
                color_4.r = 255;
                color_4.g = 255;
                barn4.GetComponent<SpriteRenderer>().color = color_4;

                color_3.b = 255;
                color_3.r = 255;
                color_3.g = 255;
                color_3.a = color_3.a - 0.05F;
                barn3.GetComponent<SpriteRenderer>().color = color_3;
                if (color_3.a <= 0F)
                {
                    color_4.a = 1F;
                    current_phase = 4;
                }
            }
            

            current_timer = 0;
        }
        else
        {
            current_timer++;
        }
	}
}
