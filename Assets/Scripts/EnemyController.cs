﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float speed = 0.65f;
    Transform trail_weat_field;

    public GameObject corn_leaf_explo;
	// Use this for initialization
	void Start () {
        speed = speed * Random.Range(0.8F, 1.2F);
        trail_weat_field = transform.Find("Trail_1");

    }
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(new Vector3(-0.1f * speed , 0, 0));

	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
       // Debug.Log("ENTER");
        if (collision.CompareTag("weat_field"))
        {
            trail_weat_field.gameObject.SetActive(true);
        }

        if (collision.CompareTag("corn_field_end"))
        {
            Instantiate(corn_leaf_explo, transform.position, Quaternion.Euler(0, -90f, 90));
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("weat_field"))
        {
            //trail_weat_field.gameObject.SetActive(false);
            trail_weat_field.transform.parent = null;
        }

    }


}
