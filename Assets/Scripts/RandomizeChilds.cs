﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeChilds : MonoBehaviour {

    public bool Randomize_flip = true;
    public bool Randomize_scale = true;
    public float scale_min=0.9f, scale_max=1.1f;

	// Use this for initialization
	void Start () {
        

        foreach (Transform child in transform)
        {
            if (Randomize_flip)
            {
                if (Random.Range(0f, 1f) > 0.5f)
                {
                    child.Rotate(0f, 180f, 0f);
                }
            }
          
            if (Randomize_scale)
            {
                float tmp_scale= child.localScale.x * Random.Range(scale_min, scale_max);
                child.localScale = new Vector3(tmp_scale, tmp_scale, tmp_scale);
            }

        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
